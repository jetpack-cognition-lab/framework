/*---------------------------------+
 | Matthias Kubisch                |
 | kubisch@informatik.hu-berlin.de |
 | November 2018 - December 2021   |
 +---------------------------------*/

#ifndef SIMPLE_TIMER_HPP
#define SIMPLE_TIMER_HPP

#include <chrono>
#include <common/log_messages.h>

class SimpleTimer {
    uint64_t timeout_us, elapsed_us;
    uint32_t max_step_us;
    std::chrono::high_resolution_clock::time_point time_0, time_1;
    bool enabled;

public:
    SimpleTimer(uint64_t timeout_us, bool enabled, uint32_t max_step_us)
    : timeout_us(timeout_us)
    , elapsed_us()
    , max_step_us(max_step_us)
    , time_0(std::chrono::high_resolution_clock::now())
    , time_1(time_0)
    , enabled(enabled)
    {
        sts_msg("creating timer with timeout[us] %llu", timeout_us);
    }

    void reset(void) { time_0 = std::chrono::high_resolution_clock::now(); elapsed_us = 0; }
    void start(void) { enabled = true; }
    void stop(void) { enabled = false; }

    bool check_if_timed_out_and_restart(uint64_t new_timeout_us) {
        timeout_us = new_timeout_us;
        return check_if_timed_out_and_restart();
    }

    bool check_if_timed_out_and_restart(void) {
        if (is_timed_out()) {
            reset();
            return true;
        } else return false;
    }

    bool is_timed_out(void) {
        if (not enabled) return false;
        time_1 = std::chrono::high_resolution_clock::now();
        int64_t step_us = std::chrono::duration_cast<std::chrono::microseconds>(time_1 - time_0).count();
        elapsed_us += check_clock_change(step_us);
        time_0 = time_1;
        return (elapsed_us >= timeout_us);
    }

    uint64_t get_elapsed_us(void) const { return elapsed_us; }
    uint64_t get_timeout_us(void) const { return timeout_us; }
    uint8_t get_elapsed_percent(void) const { return (timeout_us != 0) ? clip(100*elapsed_us/timeout_us,0,100) : 0ul; }

private:
    /* check if clock was externally set back or forward */
    inline int64_t check_clock_change(int64_t step) {
        if (step > max_step_us) {
            wrn_msg("System clock was set forward.");
            return max_step_us;
        } else if (step < 0) {
            wrn_msg("System clock was set back.");
            return 0;
        } else
            return step;
    }
};

#endif /* SIMPLE_TIMER_HPP */
